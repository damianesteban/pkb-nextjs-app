FROM node:12-alpine as builder


# Create app directory
RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app/

# Copy initial files
COPY package.json /usr/src/app/
COPY yarn.lock /usr/src/app
COPY tsconfig.json /usr/src/app
COPY next-env.d.ts /usr/src/app
COPY .babelrc /usr/src/app
RUN yarn install

# Copy over the src and config folders, compile
COPY ./pages ./pages
RUN yarn build

# Stage 2
FROM node:12-alpine

# Copy necessary files over from builder container
WORKDIR /usr/src/app
COPY package.json ./
COPY yarn.lock ./

# Do a production install
RUN yarn install --only=production

# Copy compiled code and config folder
COPY --from=builder /usr/src/app/.next ./.next

EXPOSE 3000
# Start it up
CMD yarn run start
